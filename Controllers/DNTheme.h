//
//  DNTheme.h
//  DNKit
//
//  Created by Daniel Levy on 11/12/13.
//  Copyright (c) 2013 Daniel Levy. All rights reserved.
//

@import UIKit;

@interface DNTheme : NSObject

+ (void)setAsThemeClass;
+ (Class)currentTheme;

#pragma mark - Modal Navigation Attributes

+ (BOOL)isModalNavigationBarTranslucent;
+ (UIColor *)modalBarTintColor;
+ (UIColor *)modalTintColor;
+ (UIFont *)modalFont;
+ (NSDictionary *)modalTitleTextAttributes;

#pragma mark - Navigation Attributes

+ (BOOL)isNavigationBarTranslucent;
+ (UIColor *)navigationBarTintColor;
+ (UIColor *)navigationTintColor;
+ (UIFont *)navigationFont;
+ (NSDictionary *)navigationTitleTextAttributes;

#pragma mark - Bar Theming

+ (void)themeNavigationBar:(UINavigationBar *)navigationBar;
+ (void)themeModalNavigationBar:(UINavigationBar *)navigationBar;

@end
