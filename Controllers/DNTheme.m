//
//  DNTheme.m
//  DNKit
//
//  Created by Daniel Levy on 11/12/13.
//  Copyright (c) 2013 Daniel Levy. All rights reserved.
//

#import "DNTheme.h"

@implementation DNTheme

static NSString *themeClassName = @"DNTheme";

+ (void)setAsThemeClass
{
    themeClassName = NSStringFromClass([self class]);
}

+ (Class)currentTheme
{
    return NSClassFromString(themeClassName);
}

#pragma mark - Modal Navigation Attributes

+ (BOOL)isModalNavigationBarTranslucent
{
    return [[DNTheme currentTheme] isNavigationBarTranslucent];
}

+ (UIColor *)modalBarTintColor
{
    return [[DNTheme currentTheme] navigationBarTintColor];
}

+ (UIColor *)modalTintColor
{
    return [[DNTheme currentTheme] navigationTintColor];
}

+ (UIFont *)modalFont
{
    return [[DNTheme currentTheme] navigationFont];
}

+ (NSDictionary *)modalTitleTextAttributes
{
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    if ([[DNTheme currentTheme] navigationFont] != nil)
    {
        [attributes setObject:[[DNTheme currentTheme] navigationFont] forKey:NSFontAttributeName];
    }
    [attributes setObject:[[DNTheme currentTheme] modalTintColor] forKey:NSForegroundColorAttributeName];
    return attributes;
}

#pragma mark - Navigation Attributes

+ (BOOL)isNavigationBarTranslucent
{
    NSAssert(NO, @"This is an abstract method and should be overridden");
    return NO;
}

+ (UIColor *)navigationBarTintColor
{
    NSAssert(NO, @"This is an abstract method and should be overridden");
    return nil;
}

+ (UIColor *)navigationTintColor
{
    NSAssert(NO, @"This is an abstract method and should be overridden");
    return nil;
}

+ (UIFont *)navigationFont
{
    return nil;
}

+ (NSDictionary *)navigationTitleTextAttributes
{
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    if ([[DNTheme currentTheme] navigationFont] != nil)
    {
        [attributes setObject:[[DNTheme currentTheme] navigationFont] forKey:NSFontAttributeName];
    }
    [attributes setObject:[[DNTheme currentTheme] navigationTintColor] forKey:NSForegroundColorAttributeName];
    return attributes;
}

#pragma mark - Bar Theming

+ (void)themeNavigationBar:(UINavigationBar *)navigationBar
{
    [navigationBar setTranslucent:[[DNTheme currentTheme] isNavigationBarTranslucent]];
    [navigationBar setBarTintColor:[[DNTheme currentTheme] navigationBarTintColor]];
    [navigationBar setTitleTextAttributes:[[DNTheme currentTheme] navigationTitleTextAttributes]];
    [navigationBar setTintColor:[[DNTheme currentTheme] navigationTintColor]];
}

+ (void)themeModalNavigationBar:(UINavigationBar *)navigationBar
{
    [navigationBar setTranslucent:[[DNTheme currentTheme] isModalNavigationBarTranslucent]];
    [navigationBar setBarTintColor:[[DNTheme currentTheme] modalBarTintColor]];
    [navigationBar setTitleTextAttributes:[[DNTheme currentTheme] modalTitleTextAttributes]];
    [navigationBar setTintColor:[[DNTheme currentTheme] modalTintColor]];
}

@end
