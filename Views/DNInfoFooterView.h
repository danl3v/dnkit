//
//  DNInfoFooterView.h
//  Quotes
//
//  Created by Daniel Levy on 2/11/14.
//  Copyright (c) 2014 Daniel Levy. All rights reserved.
//

@import UIKit;

@interface DNInfoFooterView : UIView

- (void)setDisclaimerLabelText:(NSString *)disclaimerLabelText;
- (void)setVersionLabelText:(NSString *)versionLabelText;
- (void)setBuildLabelText:(NSString *)buildLabelText;

- (void)setCompanyLogoImage:(UIImage *)companyLogoImage;

- (CGFloat)heightForWidth:(CGFloat)width;

@end
