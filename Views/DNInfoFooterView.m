//
//  DNInfoFooterView.m
//  Quotes
//
//  Created by Daniel Levy on 2/11/14.
//  Copyright (c) 2014 Daniel Levy. All rights reserved.
//

#import "DNInfoFooterView.h"

#import "UIView+DNAdditions.h"

#define SPACING 10.0f

@interface DNInfoFooterView ()

@property (nonatomic, strong) UIImageView *companyLogoImageView;
@property (nonatomic, strong) UILabel *disclaimerLabel;
@property (nonatomic, strong) UILabel *versionLabel;
@property (nonatomic, strong) UILabel *buildLabel;

@end

@implementation DNInfoFooterView

#pragma mark - Init

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UILabel *versionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self addSubview:versionLabel];
        [self setVersionLabel:versionLabel];
        
        UILabel *buildLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [buildLabel setAlpha:0.0f];
        [self addSubview:buildLabel];
        [self setBuildLabel:buildLabel];
        
        UIImageView *companyLogoImageView = [[UIImageView alloc] initWithImage:nil];
        [self addSubview:companyLogoImageView];
        [self setCompanyLogoImageView:companyLogoImageView];
        
        UILabel *disclaimerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [disclaimerLabel setFont:[UIFont systemFontOfSize:15.0f]];
        [disclaimerLabel setNumberOfLines:0];
        [disclaimerLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [disclaimerLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:disclaimerLabel];
        [self setDisclaimerLabel:disclaimerLabel];
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(swapLabels)];
        [self addGestureRecognizer:tapGestureRecognizer];
    }
    return self;
}

#pragma mark - Layout

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [[self versionLabel] dn_centerXInView:self];
    [[self buildLabel] dn_centerXInView:self];
    
    [[self companyLogoImageView] dn_centerXInView:self];
    [[self companyLogoImageView] dn_setOriginY:CGRectGetMaxY([[self versionLabel] frame]) + SPACING];
    
    [[self disclaimerLabel] dn_setSize:[self disclaimerLabelSizeForWidth:[self bounds].size.width]];
    
    [[self disclaimerLabel] dn_centerXInView:self];
    [[self disclaimerLabel] dn_setOriginY:CGRectGetMaxY([[self companyLogoImageView] frame]) + SPACING];
}

- (CGFloat)heightForWidth:(CGFloat)width
{
    CGFloat height = 0.0f;
    
    height += CGRectGetHeight([[self versionLabel] frame]);
    height += CGRectGetHeight([[self companyLogoImageView] frame]) + SPACING;
    height += [self disclaimerLabelSizeForWidth:width].height + SPACING;
    
    return height;
}

- (CGSize)disclaimerLabelSizeForWidth:(CGFloat)width
{
    CGSize size = [[[self disclaimerLabel] attributedText] boundingRectWithSize:CGSizeMake(width - SPACING * 2.0f, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
    return size;
}

#pragma mark - Labels

- (void)setDisclaimerLabelText:(NSString *)disclaimerLabelText
{
    [[self disclaimerLabel] setText:disclaimerLabelText];
    
    [self setNeedsLayout];
}

- (void)setVersionLabelText:(NSString *)versionLabelText
{
    [[self versionLabel] setText:versionLabelText];
    [[self versionLabel] sizeToFit];
    
    [self setNeedsLayout];
}

- (void)setBuildLabelText:(NSString *)buildLabelText
{
    [[self buildLabel] setText:buildLabelText];
    [[self buildLabel] sizeToFit];
    
    [self setNeedsLayout];
}

- (void)setCompanyLogoImage:(UIImage *)companyLogoImage
{
    [[self companyLogoImageView] setImage:companyLogoImage];
    [[self companyLogoImageView] sizeToFit];
    
    [self setNeedsLayout];
}

#pragma mark - Animation

- (void)swapLabels
{
    [UIView animateWithDuration:0.3 animations:^{
        if ([[self versionLabel] alpha] == 1.0f)
        {
            [[self versionLabel] setAlpha:0.0f];
            [[self buildLabel] setAlpha:1.0f];
        }
        else
        {
            [[self versionLabel] setAlpha:1.0f];
            [[self buildLabel] setAlpha:0.0f];
        }
    }];
}

@end
