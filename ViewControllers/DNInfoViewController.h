//
//  DNInfoViewController.h
//  DNKit
//
//  Created by Daniel Levy on 11/30/13.
//  Copyright (c) 2013 Daniel Levy. All rights reserved.
//

@import UIKit;

#define kDNInfoViewControllerCellIdentifier @"DNInfoViewControllerCellIdentifier"

/**
 The DNInfoViewController class provides a simple info view controller for an iOS app. It contains a table view, company logo, version information, and disclaimer text.
 */
@interface DNInfoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong, readonly) UITableView *tableView;

/**
 Returns a newly initialized info view controller with a completonHandler.
 This is the designated initializer for this class.
 @param completionHandler The completion handler is called when the done button is tapped in the view controller's navigation bar.
 */
- (instancetype)initWithCompletionHandler:(dispatch_block_t)completionHandler;

/**
 Subclass to provide an image to insert at the footer of the info view.
 @return A UIImage.
 */
- (UIImage *)companyLogoImage;

/**
 Subclass to provide a disclaimer string to insert at the footer of the info view.
 @return A NSString.
 */
- (NSString *)disclaimerText;

@end
