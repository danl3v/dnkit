//
//  DNInfoViewController.m
//  DNKit
//
//  Created by Daniel Levy on 11/30/13.
//  Copyright (c) 2013 Daniel Levy. All rights reserved.
//

#import "DNInfoViewController.h"

#import "DNInfoFooterView.h"
#import "DNInfoTableViewCell.h"

#import "UIView+DNAdditions.h"

#define FOOTER_BOTTOM_MARGIN 20.0f

@interface DNInfoViewController ()

@property (nonatomic, copy) dispatch_block_t completionHandler;

@property (nonatomic, strong) DNInfoFooterView *footerView;
@property (nonatomic, strong, readwrite) UITableView *tableView;

@end

@implementation DNInfoViewController

#pragma mark - Initialization

- (instancetype)initWithCompletionHandler:(dispatch_block_t)completionHandler
{
    self = [super initWithNibName:nil bundle:nil];
    if (self)
    {
        [self setCompletionHandler:completionHandler];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    
    [[self navigationItem] setTitle:[self bundleName]];
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTapped:)];
    [[self navigationItem] setRightBarButtonItem:doneItem];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    [tableView registerClass:[DNInfoTableViewCell class] forCellReuseIdentifier:kDNInfoViewControllerCellIdentifier];
    [[self view] addSubview:tableView];
    [self setTableView:tableView];
    
    DNInfoFooterView *footerView = [[DNInfoFooterView alloc] initWithFrame:CGRectZero];
    [footerView setVersionLabelText:[self versionString]];
    [footerView setBuildLabelText:[self buildString]];
    [footerView setCompanyLogoImage:[self companyLogoImage]];
    [footerView setDisclaimerLabelText:[self disclaimerText]];
    [self setFooterView:footerView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[self tableView] reloadData];
    
    if ([self selectedIndexPath] != nil)
    {
        [[self tableView] selectRowAtIndexPath:[self selectedIndexPath] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
    [[self tableView] deselectRowAtIndexPath:[self selectedIndexPath] animated:YES];
    [self setSelectedIndexPath:nil];
}

#pragma mark - Buttons

- (void)doneButtonTapped:(id)sender
{
    if ([self completionHandler] != NULL)
    {
        [self completionHandler]();
    }
}

#pragma mark - Layout

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [[self tableView] dn_setSize:[[self view] bounds].size];
    
    [self layoutFooter];
}

- (void)layoutFooter
{
    [[self footerView] dn_setWidth:[[self view] bounds].size.width];
    [[self footerView] dn_setHeight:[[self footerView] heightForWidth:[[self view] bounds].size.width] + FOOTER_BOTTOM_MARGIN];

    CGFloat topLayoutGuide = self.view.safeAreaInsets.top;
    CGFloat minumumFooterOriginY = [[self tableView] bounds].size.height - topLayoutGuide - CGRectGetHeight([[self footerView] frame]);
    
    if ([self tableViewSize].height < minumumFooterOriginY)
    {
        if ([[self footerView] superview] != [self tableView])
        {
            [[self footerView] removeFromSuperview];
            [[self tableView] addSubview:[self footerView]];
        }
        [[self footerView] dn_centerXInView:[self tableView]];
        [[self footerView] dn_setOriginY:minumumFooterOriginY];
    }
    else
    {
        if ([[self tableView] tableFooterView] != [self footerView])
        {
            [[self footerView] removeFromSuperview];
            [[self tableView] setTableFooterView:[self footerView]];
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - Strings

- (NSString *)buildString
{
    NSString *gitHash = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"GITHash"];
    return [NSString stringWithFormat:NSLocalizedString(@"Build %@", nil), gitHash];
}

- (NSString *)versionString
{
    NSString *bundleName = [self bundleName];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    
    return [NSString stringWithFormat:NSLocalizedString(@"%@ %@ (%@)", nil), bundleName, version, build];
}

- (NSString *)bundleName
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
}

#pragma mark - Subclassing

- (UIImage *)companyLogoImage
{
    return nil;
}

- (NSString *)disclaimerText
{
    return nil;
}

#pragma mark - Helpers

- (CGSize)tableViewSize
{
    CGSize tableViewSize = CGSizeZero;
    
    NSInteger numberOfSections = [[self tableView] numberOfSections];
    
    for (NSInteger section = 0; section < numberOfSections; section++)
    {
        CGRect rect = [[self tableView] rectForHeaderInSection:section];
        tableViewSize = CGSizeMake(MAX(tableViewSize.width, rect.size.width), (tableViewSize.height + rect.size.height));
        
        rect = [[self tableView] rectForSection:section];
        tableViewSize = CGSizeMake(MAX(tableViewSize.width, rect.size.width), (tableViewSize.height + rect.size.height));
        
        rect = [[self tableView] rectForFooterInSection:section];
        tableViewSize = CGSizeMake(MAX(tableViewSize.width, rect.size.width), (tableViewSize.height + rect.size.height));
    }
    return tableViewSize;
}

@end
