//
//  DNInfoTableViewCell.m
//  Quotes
//
//  Created by Daniel Levy on 8/4/14.
//  Copyright (c) 2014 Daniel Levy. All rights reserved.
//

#import "DNInfoTableViewCell.h"

@implementation DNInfoTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier];
}

@end
