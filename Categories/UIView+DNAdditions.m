//
//  UIView+DNAdditions.m
//  DNKit
//
//  Created by Daniel Levy on 9/15/13.
//  Copyright (c) 2013 Daniel Levy. All rights reserved.
//

#import "UIView+DNAdditions.h"

@implementation UIView (QTAdditions)

#pragma mark - Origin

- (void)dn_setOriginX:(CGFloat)x
{
    CGRect rect = [self frame];
    rect.origin.x = x;
    [self setFrame:rect];
}

- (void)dn_setOriginY:(CGFloat)y
{
    CGRect rect = [self frame];
    rect.origin.y = y;
    [self setFrame:rect];
}

- (void)dn_setOriginX:(CGFloat)x y:(CGFloat)y
{
    CGRect rect = [self frame];
    rect.origin.x = x;
    rect.origin.y = y;
    [self setFrame:rect];
}

- (void)dn_setOrigin:(CGPoint)origin
{
    CGRect rect = [self frame];
    rect.origin = origin;
    [self setFrame:rect];
}

#pragma mark - Increasing Origin

- (void)dn_increaseOriginX:(CGFloat)width
{
    CGRect rect = [self frame];
    rect.origin.x += width;
    [self setFrame:rect];
}

- (void)dn_increaseOriginY:(CGFloat)height
{
    CGRect rect = [self frame];
    rect.origin.y += height;
    [self setFrame:rect];
}

- (void)dn_increaseOriginX:(CGFloat)width y:(CGFloat)height
{
    CGRect rect = [self frame];
    rect.origin.x += width;
    rect.origin.y += height;
    [self setFrame:rect];
}

- (void)dn_increaseOrigin:(CGSize)size
{
    CGRect rect = [self frame];
    rect.origin.x += size.width;
    rect.origin.y += size.height;
    [self setFrame:rect];
}

#pragma mark - Sizing

- (void)dn_setWidth:(CGFloat)width
{
    CGRect rect = [self frame];
    rect.size.width = width;
    [self setFrame:rect];
}

- (void)dn_setHeight:(CGFloat)height
{
    CGRect rect = [self frame];
    rect.size.height = height;
    [self setFrame:rect];
}

- (void)dn_setWidth:(CGFloat)width height:(CGFloat)height
{
    CGRect rect = [self frame];
    rect.size.width = width;
    rect.size.height = height;
    [self setFrame:rect];
}

- (void)dn_setSize:(CGSize)size
{
    CGRect rect = [self frame];
    rect.size = size;
    [self setFrame:rect];
}

#pragma mark - Increasing Size

- (void)dn_increaseWidth:(CGFloat)width
{
    CGRect rect = [self frame];
    rect.size.width += width;
    [self setFrame:rect];
}

- (void)dn_increaseHeight:(CGFloat)height
{
    CGRect rect = [self frame];
    rect.size.height += height;
    [self setFrame:rect];
}

- (void)dn_increaseWidth:(CGFloat)width height:(CGFloat)height
{
    CGRect rect = [self frame];
    rect.size.width += width;
    rect.size.height += height;
    [self setFrame:rect];
}

- (void)dn_increaseSize:(CGSize)size
{
    CGRect rect = [self frame];
    rect.size.width += size.width;
    rect.size.height += size.height;
    [self setFrame:rect];
}

#pragma mark - Centering

- (void)dn_centerXInView:(UIView *)view
{
    CGRect frameInReceiverSuperviewCoordinateSystem = [[view layer] convertRect:[view bounds] toLayer:[[self layer] superlayer]];
    
    CGRect frame = [self frame];
    frame.origin.x = [UIView dn_snapFloatToPixelBoundaries:(CGRectGetMidX(frameInReceiverSuperviewCoordinateSystem) - CGRectGetMidX([self bounds]))];
    [self setFrame:frame];
}

- (void)dn_centerYInView:(UIView *)view
{
    CGRect frameInReceiverSuperviewCoordinateSystem = [[view layer] convertRect:[view bounds] toLayer:[[self layer] superlayer]];
    
    CGRect frame = [self frame];
    frame.origin.y = [UIView dn_snapFloatToPixelBoundaries:(CGRectGetMidY(frameInReceiverSuperviewCoordinateSystem) - CGRectGetMidY([self bounds]))];
    [self setFrame:frame];
}

- (void)dn_centerInView:(UIView *)view
{
    CGRect frameInReceiverSuperviewCoordinateSystem = [[view layer] convertRect:[view bounds] toLayer:[[self layer] superlayer]];
    
    CGRect frame = [self frame];
    frame.origin.x = [UIView dn_snapFloatToPixelBoundaries:(CGRectGetMidX(frameInReceiverSuperviewCoordinateSystem) - CGRectGetMidX([self bounds]))];
    frame.origin.y = [UIView dn_snapFloatToPixelBoundaries:(CGRectGetMidY(frameInReceiverSuperviewCoordinateSystem) - CGRectGetMidY([self bounds]))];
    [self setFrame:frame];
}

#pragma mark - Clamping

- (void)dn_clampWidth:(CGFloat)width
{
    CGRect frame = [self frame];
    if (frame.size.width > width)
    {
        frame.size.width = width;
        [self setFrame:frame];
    }
}

- (void)dn_clampHeight:(CGFloat)height
{
    CGRect frame = [self frame];
    if (frame.size.height > height)
    {
        frame.size.height = height;
        [self setFrame:frame];
    }
}

- (void)dn_clampWidth:(CGFloat)width height:(CGFloat)height
{
    BOOL clamped = NO;
    
    CGRect frame = [self frame];
    if (frame.size.width > width)
    {
        frame.size.width = width;
        clamped = YES;
    }
    
    if (frame.size.height > height)
    {
        frame.size.height = height;
        clamped = YES;
    }
    
    if (clamped)
    {
        [self setFrame:frame];
    }
}

- (void)dn_clampSize:(CGSize)size
{
    BOOL clamped = NO;
    
    CGRect frame = [self frame];
    if (frame.size.width > size.width)
    {
        frame.size.width = size.width;
        clamped = YES;
    }
    
    if (frame.size.height > size.height)
    {
        frame.size.height = size.height;
        clamped = YES;
    }
    
    if (clamped)
    {
        [self setFrame:frame];
    }
}

#pragma mark - Snapping

+ (CGFloat)dn_snapFloatDownToPixelBoundaries:(CGFloat)aFloat
{
    return [self dn_snapFloatDownToPixelBoundaries:aFloat scale:[[UIScreen mainScreen] scale]];
}

+ (CGFloat)dn_snapFloatUpToPixelBoundaries:(CGFloat)aFloat
{
    return [self dn_snapFloatUpToPixelBoundaries:aFloat scale:[[UIScreen mainScreen] scale]];
}

+ (CGFloat)dn_snapFloatToPixelBoundaries:(CGFloat)aFloat
{
    return [self dn_snapFloatToPixelBoundaries:aFloat scale:[[UIScreen mainScreen] scale]];
}

+ (CGRect)dn_snapRectToPixelBoundaries:(CGRect)aRect
{
    return [self dn_snapRectToPixelBoundaries:aRect scale:[[UIScreen mainScreen] scale]];
}

+ (CGFloat)dn_snapFloatDownToPixelBoundaries:(CGFloat)aFloat scale:(CGFloat)scale
{
    return floorf(aFloat * scale) / scale;
}

+ (CGFloat)dn_snapFloatUpToPixelBoundaries:(CGFloat)aFloat scale:(CGFloat)scale
{
    return ceilf(aFloat * scale) / scale;
}

+ (CGFloat)dn_snapFloatToPixelBoundaries:(CGFloat)aFloat scale:(CGFloat)scale
{
    return roundf(aFloat * scale) / scale;
}

+ (CGRect)dn_snapRectToPixelBoundaries:(CGRect)aRect scale:(CGFloat)scale
{
    CGRect newRect = CGRectZero;
    newRect.origin.x = [self dn_snapFloatToPixelBoundaries:aRect.origin.x scale:scale];
    newRect.origin.y = [self dn_snapFloatToPixelBoundaries:aRect.origin.y scale:scale];
    newRect.size.width = [self dn_snapFloatToPixelBoundaries:aRect.size.width scale:scale];
    newRect.size.height = [self dn_snapFloatToPixelBoundaries:aRect.size.height scale:scale];
    return newRect;
}

@end
