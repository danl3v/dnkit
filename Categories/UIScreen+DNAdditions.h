//
//  UIScreen+DNAdditions.h
//  DNKit
//
//  Created by Daniel Levy on 11/12/13.
//  Copyright (c) 2013 Daniel Levy. All rights reserved.
//

@import UIKit;

/**
 This category adds some helper methods.
 */
@interface UIScreen (DNAdditions)

/**
 Returns a float indicating of points per pixel for the current screen.
 */
+ (CGFloat)dn_onePixel;

@end
