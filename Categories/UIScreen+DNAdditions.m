//
//  UIScreen+DNAdditions.m
//  DNKit
//
//  Created by Daniel Levy on 11/12/13.
//  Copyright (c) 2013 Daniel Levy. All rights reserved.
//

#import "UIScreen+DNAdditions.h"

@implementation UIScreen (DNAdditions)

+ (CGFloat)dn_onePixel
{
    return (1.0f / [[UIScreen mainScreen] scale]);
}

@end
