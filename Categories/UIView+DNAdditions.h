//
//  UIView+DNAdditions.h
//  DNKit
//
//  Created by Daniel Levy on 9/15/13.
//  Copyright (c) 2013 Daniel Levy. All rights reserved.
//

@import UIKit;

/**
 This category adds methods that help adjust a view's frame.
 */
@interface UIView (DNAdditions)

#pragma mark - Origin

/**
 Sets the x-coordinate of the receiver's frame.
 @param x The new x-coordinate to assign.
 */
- (void)dn_setOriginX:(CGFloat)x;

/**
 Sets the y-coordinate of the receiver's frame.
 @param y The new y-coordinate to assign.
 */
- (void)dn_setOriginY:(CGFloat)y;

/**
 Sets the x-coordinate and y-coordinate of the receiver's frame.
 @param x The new x-coordinate to assign.
 @param y The new y-coordinate to assign.
 */
- (void)dn_setOriginX:(CGFloat)x y:(CGFloat)y;

/**
 Sets the origin of the receiver's frame.
 @param origin The new origin to assign.
 */
- (void)dn_setOrigin:(CGPoint)origin;

#pragma mark - Increasing Origin

/**
 Increases the x-coordinate of the receiver's frame.
 @param width The amount to increase the receiver's x-coordinate.
 */
- (void)dn_increaseOriginX:(CGFloat)width;

/**
 Increases the y-coordinate of the receiver's frame.
 @param height The amount to increase the receiver's y-coordinate.
 */
- (void)dn_increaseOriginY:(CGFloat)height;

/**
 Increases the x- and y-coordinate of the receiver's frame.
 @param width The amount to increase the receiver's x-coordinate.
 @param height The amount to increase the receiver's y-coordinate.
 */
- (void)dn_increaseOriginX:(CGFloat)width y:(CGFloat)height;

/**
 Increases the origin of the receiver's frame.
 @param size The size to increase the receiver's origin.
 */
- (void)dn_increaseOrigin:(CGSize)size;

#pragma mark - Sizing

/**
 Sets the width of the receiver's frame.
 @param width The new width to assign.
 */
- (void)dn_setWidth:(CGFloat)width;

/**
 Sets the height of the receiver's frame.
 @param height The new height to assign.
 */
- (void)dn_setHeight:(CGFloat)height;

/**
 Sets the width and height of the receiver's frame.
 @param width The new width to assign.
 @param height The new height to assign.
 */
- (void)dn_setWidth:(CGFloat)width height:(CGFloat)height;

/**
 Sets the size of the receiver's frame.
 @param size The new size to assign.
 */
- (void)dn_setSize:(CGSize)size;

#pragma mark - Increasing Size

/**
 Increases the width of the receiver's frame.
 @param width The amount to increase the receiver's width.
 */
- (void)dn_increaseWidth:(CGFloat)width;

/**
 Increases the height of the receiver's height.
 @param height The amount to increase the receiver's height.
 */
- (void)dn_increaseHeight:(CGFloat)height;

/**
 Increases the width and height of the receiver's frame.
 @param width The amount to increase the receiver's width.
 @param height The amount to increase the receiver's height.
 */
- (void)dn_increaseWidth:(CGFloat)width height:(CGFloat)height;

/**
 Increases the size of the receiver's frame.
 @param size The size to increase the receiver's frame.
 */
- (void)dn_increaseSize:(CGSize)size;

#pragma mark - Centering

/**
 Centers the receiver horizontally in view's frame. The x-coordinate is adjusted to lie on pixel boundaries for the current device.
 @param view The view in which to horizontally center the receiver.
 */
- (void)dn_centerXInView:(UIView *)view;

/**
 Centers the receiver vertically in view's frame. The y-coordinate is adjusted to lie on pixel boundaries for the current device.
 @param view The view in which to vertically center the receiver.
 */
- (void)dn_centerYInView:(UIView *)view;

/**
 Centers the receiver in view's frame. The x- and y-coordinate are adjusted to lie on pixel boundaries for the current device.
 @param view The view in which to center the receiver.
 */
- (void)dn_centerInView:(UIView *)view;

#pragma mark - Clamping

/**
 Sets a maximum width on the receiver's frame.
 @param width The maximum width for the receiver.
 */
- (void)dn_clampWidth:(CGFloat)width;

/**
 Sets a maximum height on the receiver's frame.
 @param height The maximum height for the receiver.
 */
- (void)dn_clampHeight:(CGFloat)height;

/**
 Sets a maximum width and height on the receiver's frame.
 @param width The maximum width for the receiver.
 @param height The maximum height for the receiver.
 */
- (void)dn_clampWidth:(CGFloat)width height:(CGFloat)height;

/**
 Sets a maximum size on the receiver's frame.
 @param size The maximum size for the receiver.
 */
- (void)dn_clampSize:(CGSize)size;

#pragma mark - Snapping

/**
 Rounds down a float if necessary so that it lies on a pixel boundary for the current screen's scale
 @param aFloat The float to snap down to a pixel boundary.
 */
+ (CGFloat)dn_snapFloatDownToPixelBoundaries:(CGFloat)aFloat;

/**
 Rounds up a float if necessary so that it lies on a pixel boundary for the current screen's scale
 @param aFloat The float to snap up to a pixel boundary.
 */
+ (CGFloat)dn_snapFloatUpToPixelBoundaries:(CGFloat)aFloat;

/**
 Rounds a float if necessary so that it lies on a pixel boundary for the current screen's scale
 @param aFloat The float to round to a pixel boundary.
 */
+ (CGFloat)dn_snapFloatToPixelBoundaries:(CGFloat)aFloat;

/**
 Rounds a rect if necessary so that it lies on pixel boundaries for the current screen's scale
 @param aRect The rect to round to pixel boundaries.
 */
+ (CGRect)dn_snapRectToPixelBoundaries:(CGRect)aRect;

/**
 Rounds down a float if necessary so that it lies on a pixel boundary for the given scale
 @param aFloat The float to snap down to a pixel boundary.
 @param scale The scale at which to snap to.
 */
+ (CGFloat)dn_snapFloatDownToPixelBoundaries:(CGFloat)aFloat scale:(CGFloat)scale;

/**
 Rounds up a float if necessary so that it lies on a pixel boundary for the given scale
 @param aFloat The float to snap up to a pixel boundary.
 @param scale The scale at which to snap to.
 */
+ (CGFloat)dn_snapFloatUpToPixelBoundaries:(CGFloat)aFloat scale:(CGFloat)scale;

/**
 Rounds a float if necessary so that it lies on a pixel boundary for the given scale
 @param aFloat The float to round to a pixel boundary.
 @param scale The scale at which to snap to.
 */
+ (CGFloat)dn_snapFloatToPixelBoundaries:(CGFloat)aFloat scale:(CGFloat)scale;

/**
 Rounds a rect if necessary so that it lies on pixel boundaries for the given scale
 @param aRect The rect to round to pixel boundaries.
 @param scale The scale at which to snap to.
 */
+ (CGRect)dn_snapRectToPixelBoundaries:(CGRect)aRect scale:(CGFloat)scale;

@end
